package snowysapps.com.snowutils;

/**
 * Created by Marek Śnieżek on 18.11.2016.
 */

public class ValidationRegex {

    public static String EMPTY = "^$";
    public static String POSTAL_CODE = "[0-9]{2}-[0-9]{3}";
    public static String EMAIL_ADDRESS = "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
            "\\@" +
            "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
            "(" +
            "\\." +
            "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
            ")+";
    public static String USERNAME = "^[a-z0-9_-]{3,16}$";
    public static String PASSWORD = "^[a-z0-9_-]{6,18}$";

    /**
     * Description
     * This RE validates alpha characters that evaluate to Roman numerials, ranging from 1(I) - 3999(MMMCMXCIX). Not case sensitive.
     */
    public static String ROMAN_NUMERIALS = "^(?i:(?=[MDCLXVI])((M{0,3})((C[DM])|(D?C{0,3}))?((X[LC])|(L?XX{0,2})|L)?((I[VX])|(V?(II{0,2}))|V)?))$";
}
